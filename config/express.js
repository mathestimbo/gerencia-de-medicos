const express = require('express');
const load = require('express-load');
const bodyParser = require('body-parser');
const session = require('express-session');

module.exports = () => {
    const app = express();

    app.set('view engine', 'ejs');
    app.set('views', './app/views');

    app.use(bodyParser.urlencoded({extended:true}));
    app.use(express.static('public'));
    app.use(session({
        secret : 'medico',
        resave : false,
        saveUninitialized : true
    }));

    load('routes', {cwd:'app'})
        .then('db')
        .into(app);

    return app;
    
}