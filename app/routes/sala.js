module.exports = app => {
    app.get('/sala', (req, res) => {
        if (req.session.usuario != "") {
            const connection = app.db.connectionFactory();
            const salaDAO = new app.db.SalaDAO(connection);

            salaDAO.read((errors, results) => {
                res.render('sala', { 
                    salas: results,
                    usuario : req.session.usuario,
                    tipo : req.session.tipo
                });
            })

            connection.end();
        } else {
            res.redirect('/')
        }
    });

    app.post('/sala/novo', (req, res) => {
        const connection = app.db.connectionFactory();
        const salaDAO = new app.db.SalaDAO(connection);

        const sala = req.body;

        salaDAO.create(sala, (errors, results) => {
            res.redirect('/sala');
        })

        connection.end();
    })

    app.post('/sala/edita', (req, res) => {
        const connection = app.db.connectionFactory();
        const salaDAO = new app.db.SalaDAO(connection);

        const sala = req.body;

        salaDAO.update(sala, (errors, results) => {
            res.redirect('/sala');
        })

        connection.end();
    })

    app.post('/sala/deleta', (req, res) => {
        const connection = app.db.connectionFactory();
        const salaDAO = new app.db.SalaDAO(connection);

        const id = req.body.rowId;

        salaDAO.delete(id, (errors, results) => {
            res.redirect('/sala');
        })

        connection.end();
    })
}