module.exports = app => {
    app.get('/', (req, res) => {
        res.render('login');
    });

    app.get('/logoff', (req, res) => {
        delete req.session.usuario;
        delete req.session.senha;
        delete req.session.tipo;
        res.render('login');
    });

    app.post('/', (req, res, next) => {
        const connection = app.db.connectionFactory();
        const medicoDAO = new app.db.MedicoDAO(connection);
        const gerenteDAO = new app.db.GerenteDAO(connection);

        const loginInfo = req.body;

        const checkLoginInfo = (dao, tipoDeUsuario) => {
            dao.read((errors, results) => {
                let authOk = false;
                results.forEach(result => {
                    if (result['usuario'] == req.body['usuario'] && result['senha'] == req.body['senha']) {
                        req.session.usuario = result['usuario'];
                        req.session.senha = result['senha'];
                        req.session.tipo = tipoDeUsuario;
                        authOk = true;
                    } 
                });
                authOk ? res.redirect('/cirurgia') : next();
            })
        }

        if (req.body['tipo'] == 'medico') {
            checkLoginInfo(medicoDAO, 'medico');
        } else {
            checkLoginInfo(gerenteDAO, 'gerente')
        }

    }, (req, res) => res.redirect('/'))
}