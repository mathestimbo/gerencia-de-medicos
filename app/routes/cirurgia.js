module.exports = app => {
    app.get('/cirurgia', (req,res) => {
        if(req.session.usuario != "") {
            const connection = app.db.connectionFactory();
            const cirurgiaDAO = new app.db.CirurgiaDAO(connection);
            const medicoDAO = new app.db.MedicoDAO(connection);
            const clienteDAO = new app.db.ClienteDAO(connection);
            const salaDAO = new app.db.SalaDAO(connection);

            let clientes;
            clienteDAO.read((errors, results) => {clientes = results});
            let medicos;
            medicoDAO.read((errors, results) => {medicos = results});
            let salas;
            salaDAO.read((errors, results) => {salas = results});

            
            cirurgiaDAO.read((errors, results) => {
                res.render('cirurgia', {
                    cirurgias : results,
                    clientes : clientes,
                    medicos : medicos,
                    salas: salas,
                    usuario : req.session.usuario,
                    tipo : req.session.tipo
                });
            })

            connection.end();
        } else {
            res.redirect('/');
        }
    });

    app.post('/cirurgia/novo', (req, res) => {
        const connection = app.db.connectionFactory();
        const cirurgiaDAO = new app.db.CirurgiaDAO(connection);

        const cirurgia = req.body;
        
        cirurgiaDAO.create(cirurgia, (errors, results) => {
            res.redirect('/cirurgia');
        })

        connection.end();
    })

    app.post('/cirurgia/edita', (req, res) => {
        const connection = app.db.connectionFactory();
        const cirurgiaDAO = new app.db.CirurgiaDAO(connection);

        const cirurgia = req.body;

        cirurgiaDAO.update(cirurgia, (errors, results) => {
            res.redirect('/cirurgia');
        })

        connection.end();
    })

    app.post('/cirurgia/deleta', (req, res) => {
        const connection = app.db.connectionFactory();
        const cirurgiaDAO = new app.db.CirurgiaDAO(connection);
        
        const id = req.body.rowId;

        cirurgiaDAO.delete(id, (errors, results) => {
            res.redirect('/cirurgia');
        })

        connection.end();
    })
}