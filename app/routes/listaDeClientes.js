module.exports = app => {
    app.get('/cliente', (req, res) => {
        if (req.session.usuario != "") {
            const connection = app.db.connectionFactory();
            const clienteDAO = new app.db.ClienteDAO(connection);

            clienteDAO.read((errors, results) => {
                res.render('cliente', { 
                    clientes: results ,
                    usuario: req.session.usuario,
                    tipo : req.session.tipo
                });
            })

            connection.end();
        } else {
            res.redirect('/');
        }
    });

    app.post('/cliente/novo', (req, res) => {
        const connection = app.db.connectionFactory();
        const clienteDAO = new app.db.ClienteDAO(connection);

        const cliente = req.body;

        clienteDAO.create(cliente, (errors, results) => {
            res.redirect('/cliente');
        })

        connection.end();
    })

    // app.post('/cliente/edita', (req, res) => {
    //     const connection = app.db.connectionFactory();
    //     const clienteDAO = new app.db.ClienteDAO(connection);

    //     const cliente = req.body;

    //     clienteDAO.update(cliente, (errors, results) => {
    //         res.redirect('/cliente');
    //     })

    //     connection.end();
    // })

    // app.post('/cliente/deleta', (req, res) => {
    //     const connection = app.db.connectionFactory();
    //     const clienteDAO = new app.db.ClienteDAO(connection);

    //     const id = req.body.rowId;

    //     clienteDAO.delete(id, (errors, results) => {
    //         res.redirect('/cliente');
    //     })

    //     connection.end();
    // })
}