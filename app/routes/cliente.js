module.exports = app => {
    app.get('/cliente', (req, res) => {
        if (req.session.usuario != "") {
            const connection = app.db.connectionFactory();
            const clienteDAO = new app.db.ClienteDAO(connection);
            const medicoDAO = new app.db.MedicoDAO(connection);

            let medicos;
            medicoDAO.read((errors, results) => medicos = results);

            clienteDAO.read((errors, results) => {
                res.render('cliente', {
                    clientes: results,
                    medicos: medicos,
                    usuario : req.session.usuario,
                    tipo : req.session.tipo
                });
            })

            connection.end();
        } else {
            res.redirect('/');
        }
    });

    app.post('/cliente/novo', (req, res) => {
        const connection = app.db.connectionFactory();
        const clienteDAO = new app.db.ClienteDAO(connection);
        const listaDeClientesDAO = new app.db.ListaDeClientesDAO(connection);

        const clienteComMedico = req.body;
        const cliente = Object.assign({}, clienteComMedico, { medico_idmedico: undefined });
        delete cliente.medico_idmedico;

        let idCliente;

        clienteDAO.create(cliente, (errors, results) => {
            idCliente = results['insertId'];
        })

        const itemDaLista = {
            medico_idmedico: clienteComMedico.medico_idmedico,
            cliente_idcliente: idCliente
        }

        listaDeClientesDAO.create(itemDaLista, (errors, results) => {
            res.redirect('/cliente');
        })

        connection.end();
    })

    app.post('/cliente/edita', (req, res) => {
        const connection = app.db.connectionFactory();
        const clienteDAO = new app.db.ClienteDAO(connection);

        const cliente = req.body;

        clienteDAO.update(cliente, (errors, results) => {
            res.redirect('/cliente');
        })

        connection.end();
    })

    app.post('/cliente/deleta', (req, res) => {
        const connection = app.db.connectionFactory();
        const clienteDAO = new app.db.ClienteDAO(connection);

        const id = req.body.rowId;

        clienteDAO.delete(id, (errors, results) => {
            res.redirect('/cliente');
        })

        connection.end();
    })
}