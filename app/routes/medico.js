module.exports = app => {
    app.get('/medico', (req,res) => {
        if(req.session.usuario != "") {
            const connection = app.db.connectionFactory();
            const medicoDAO = new app.db.MedicoDAO(connection);
            const listaDeClientesDAO = new app.db.ListaDeClientesDAO(connection);
            
            let listaDeClientes;
            listaDeClientesDAO.read((errors, results) => listaDeClientes = results);
            
            medicoDAO.read((errors, results) => {
                res.render('medico', {
                    medicos : results,
                    listaDeClientes : listaDeClientes,
                    usuario : req.session.usuario,
                    tipo : req.session.tipo
                });
            })
            connection.end();
        } else {
            res.redirect('/');
        }
    });

    app.post('/medico/novo', (req, res) => {
        const connection = app.db.connectionFactory();
        const medicoDAO = new app.db.MedicoDAO(connection);

        const medico = req.body;
        
        medicoDAO.create(medico, (errors, results) => {
            res.redirect('/medico');
        })

        connection.end();
    })

    app.post('/medico/edita', (req, res) => {
        const connection = app.db.connectionFactory();
        const medicoDAO = new app.db.MedicoDAO(connection);

        const medico = req.body;

        medicoDAO.update(medico, (errors, results) => {
            res.redirect('/medico');
        })

        connection.end();
    })

    app.post('/medico/deleta', (req, res) => {
        const connection = app.db.connectionFactory();
        const medicoDAO = new app.db.MedicoDAO(connection);
        
        const id = req.body.rowId;

        medicoDAO.delete(id, (errors, results) => {
            res.redirect('/medico');
        })

        connection.end();
    })
}