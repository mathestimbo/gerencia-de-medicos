const mysql = require('mysql');

const createDBConnection = () => {
    return mysql.createConnection({
        host : 'localhost',
        user : 'jeffrey',
        password : 'password',
        database : 'hospital'
    })
}

module.exports = () => createDBConnection