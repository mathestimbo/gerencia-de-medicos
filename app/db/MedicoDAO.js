class MedicoDAO {
    constructor(connection) {
        this._connection = connection;
    }

    create(medico, callback) {
        this._connection.query(`INSERT INTO medico SET ?`, medico, callback);
    }

    read(callback) {
        this._connection.query(`SELECT idmedico, nome, DATE_FORMAT(nascimento,'%d') AS nascimentoDia,
        DATE_FORMAT(nascimento,'%m') AS nascimentoMes, DATE_FORMAT(nascimento,'%Y') AS nascimentoAno,
         crm, salario, usuario, senha FROM medico`, callback);
    }

    update(medico, callback) {
        this._connection.query(`UPDATE medico SET ? WHERE idmedico = ${medico.idmedico}`, medico, callback)
    }

    delete(rowId, callback) {
        this._connection.query(`DELETE FROM medico WHERE idmedico = ${rowId}`, callback);
    }
}

module.exports = () => {
    return MedicoDAO;
}