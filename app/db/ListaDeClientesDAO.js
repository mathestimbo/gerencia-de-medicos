class ListaDeClientesDAO {
    constructor(connection) {
        this._connection = connection;
    }

    create(lista_de_clientes, callback) {
        this._connection.query(`INSERT INTO lista_de_clientes SET ?`, lista_de_clientes, callback);
    }

    read(callback) {
        this._connection.query(`SELECT medico.nome AS medico, cliente.nome AS cliente
                                FROM hospital.lista_de_clientes
                                JOIN medico ON lista_de_clientes.medico_idmedico = medico.idmedico
                                JOIN cliente ON lista_de_clientes.cliente_idcliente = cliente.idcliente;`, callback);
    }

    // update(lista_de_clientes, callback) {
    //     this._connection.query(`UPDATE lista_de_clientes SET ? WHERE medico_idmedico = ${medico_idmedico} AND cliente_idcliente = ${cliente_idcliente}`, lista_de_clientes, callback)
    // }

    // delete(idMedico, idCliente, callback) {
    //     this._connection.query(`DELETE FROM lista_de_clientes WHERE lista_de_clientes.medico_idmedico = ${idMedico} AND lista_de_clientes.cliente_idcliente = ${idCliente}`, callback);
    // }
}

module.exports = () => {
    return ListaDeClientesDAO;
}