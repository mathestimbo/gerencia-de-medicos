class SalaDAO {
    constructor(connection) {
        this._connection = connection;
    }

    create(sala, callback) {
        this._connection.query(`INSERT INTO sala SET ?`, sala, callback);
    }

    read(callback) {
        this._connection.query(`SELECT * FROM sala`, callback);
    }

    update(sala, callback) {
        this._connection.query(`UPDATE sala SET ? WHERE idsala = ${sala.idsala}`, sala, callback)
    }

    delete(rowId, callback) {
        this._connection.query(`DELETE FROM sala WHERE idsala = ${rowId}`, callback);
    }
}

module.exports = () => {
    return SalaDAO;
}