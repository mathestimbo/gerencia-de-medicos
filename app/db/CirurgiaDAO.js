class CirurgiaDAO {
    constructor(connection) {
        this._connection = connection;
    }

    create(cirurgia, callback) {
        this._connection.query(`INSERT INTO cirurgia SET ?`, cirurgia, callback);
    }

    read(callback) {
        this._connection.query(`SELECT cirurgia.idcirurgia, cliente.nome AS cliente,
                                        medico.nome AS medico, sala.numero AS sala,
                                        cliente.idcliente, sala.idsala, medico.idmedico,
                                        date_format(cirurgia.data, '%d') AS dia,
                                        date_format(cirurgia.data, '%m') AS mes,
                                        date_format(cirurgia.data, '%Y') AS ano,
                                        cirurgia.hora,
                                        cirurgia.feedback
                                FROM cirurgia
                                JOIN cliente ON cirurgia.cliente_idcliente = cliente.idcliente
                                JOIN medico ON cirurgia.medico_idmedico = medico.idmedico
                                JOIN sala ON cirurgia.sala_idsala = sala.idsala`, callback);
    }

    update(cirurgia, callback) {
        this._connection.query(`UPDATE cirurgia SET ? WHERE idcirurgia = ${cirurgia.idcirurgia}`, cirurgia, callback)
    }

    delete(rowId, callback) {
        this._connection.query(`DELETE FROM cirurgia WHERE idcirurgia = ${rowId}`, callback);
    }
}

module.exports = () => {
    return CirurgiaDAO;
}