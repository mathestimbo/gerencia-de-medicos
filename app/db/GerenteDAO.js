class GerenteDAO {
    constructor(connection) {
        this._connection = connection;
    }

    // create(gerente, callback) {
    //     this._connection.query(`INSERT INTO gerente SET ?`, gerente, callback);
    // }

    read(callback) {
        this._connection.query(`SELECT idgerente, nome, DATE_FORMAT(nascimento,'%d') AS nascimentoDia,
        DATE_FORMAT(nascimento,'%m') AS nascimentoMes, DATE_FORMAT(nascimento,'%Y') AS nascimentoAno,
         matricula, salario, usuario, senha FROM gerente`, callback);
    }

    // update(gerente, callback) {
    //     this._connection.query(`UPDATE gerente SET ? WHERE idgerente = ${gerente.idgerente}`, gerente, callback)
    // }

    // delete(rowId, callback) {
    //     this._connection.query(`DELETE FROM gerente WHERE idgerente = ${rowId}`, callback);
    // }
}

module.exports = () => {
    return GerenteDAO;
}