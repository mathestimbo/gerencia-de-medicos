class ClienteDAO {
    constructor(connection) {
        this._connection = connection;
    }

    create(cliente, callback) {
        this._connection.query(`INSERT INTO cliente SET ?`, cliente, callback);
    }

    read(callback) {
        this._connection.query(`SELECT idcliente, nome, DATE_FORMAT(nascimento,'%d') AS nascimentoDia,
        DATE_FORMAT(nascimento,'%m') AS nascimentoMes, DATE_FORMAT(nascimento,'%Y') AS nascimentoAno, cpf FROM cliente`, callback);
    }

    update(cliente, callback) {
        this._connection.query(`UPDATE cliente SET ? WHERE idcliente = ${cliente.idcliente}`, cliente, callback)
    }

    delete(rowId, callback) {
        this._connection.query(`DELETE FROM cliente WHERE idcliente = ${rowId}`, callback);
    }
}

module.exports = () => {
    return ClienteDAO;
}