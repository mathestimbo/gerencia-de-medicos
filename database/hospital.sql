CREATE DATABASE  IF NOT EXISTS `hospital` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `hospital`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: hospital
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cirurgia`
--

DROP TABLE IF EXISTS `cirurgia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cirurgia` (
  `idcirurgia` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_idcliente` int(11) NOT NULL,
  `medico_idmedico` int(11) NOT NULL,
  `sala_idsala` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `feedback` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idcirurgia`,`cliente_idcliente`,`medico_idmedico`,`sala_idsala`),
  KEY `fk_cirurgia_cliente_idx` (`cliente_idcliente`),
  KEY `fk_cirurgia_medico1_idx` (`medico_idmedico`),
  KEY `fk_cirurgia_sala1_idx` (`sala_idsala`),
  CONSTRAINT `fk_cirurgia_cliente` FOREIGN KEY (`cliente_idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cirurgia_medico1` FOREIGN KEY (`medico_idmedico`) REFERENCES `medico` (`idmedico`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cirurgia_sala1` FOREIGN KEY (`sala_idsala`) REFERENCES `sala` (`idsala`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cirurgia`
--

LOCK TABLES `cirurgia` WRITE;
/*!40000 ALTER TABLE `cirurgia` DISABLE KEYS */;
INSERT INTO `cirurgia` VALUES (1,1,1,1,'2018-12-12','09:00:00','Muito boa');
/*!40000 ALTER TABLE `cirurgia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `cpf` char(11) DEFAULT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Daniel Chagas','1976-12-04','11111111111'),(3,'Marcos','2018-12-05','441241240'),(4,'leo','1996-04-26','123123'),(8,'Carol','2006-06-06','666');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gerente`
--

DROP TABLE IF EXISTS `gerente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `gerente` (
  `idgerente` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `nascimento` date NOT NULL,
  `matricula` int(11) NOT NULL,
  `salario` double NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  PRIMARY KEY (`idgerente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gerente`
--

LOCK TABLES `gerente` WRITE;
/*!40000 ALTER TABLE `gerente` DISABLE KEYS */;
INSERT INTO `gerente` VALUES (1,'Leonardo','1996-04-26',1,25000,'leo','123'),(2,'rodrigo','1995-05-08',2,135135,'rod','321');
/*!40000 ALTER TABLE `gerente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_de_clientes`
--

DROP TABLE IF EXISTS `lista_de_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lista_de_clientes` (
  `medico_idmedico` int(11) NOT NULL,
  `cliente_idcliente` int(11) NOT NULL,
  PRIMARY KEY (`medico_idmedico`,`cliente_idcliente`),
  KEY `fk_medico_has_cliente_cliente1_idx` (`cliente_idcliente`),
  KEY `fk_medico_has_cliente_medico1_idx` (`medico_idmedico`),
  CONSTRAINT `fk_medico_has_cliente_cliente1` FOREIGN KEY (`cliente_idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_medico_has_cliente_medico1` FOREIGN KEY (`medico_idmedico`) REFERENCES `medico` (`idmedico`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_de_clientes`
--

LOCK TABLES `lista_de_clientes` WRITE;
/*!40000 ALTER TABLE `lista_de_clientes` DISABLE KEYS */;
INSERT INTO `lista_de_clientes` VALUES (1,1),(2,3),(2,8);
/*!40000 ALTER TABLE `lista_de_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local`
--

DROP TABLE IF EXISTS `local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `nome_local` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tipo_local` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `ultimo_ping` datetime NOT NULL,
  `versao` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nao` int(11) NOT NULL,
  `regional` int(11) NOT NULL,
  `aband` int(11) NOT NULL,
  `operadora` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `num_fone` int(11) NOT NULL,
  `serial_sim` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `versao_alvo` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local`
--

LOCK TABLES `local` WRITE;
/*!40000 ALTER TABLE `local` DISABLE KEYS */;
INSERT INTO `local` VALUES (8,'UAPS Anastácio Magalhães',1,-38.5525,-3.74592,'2018-12-04 15:45:09','115L',3460,3,1976,'',0,'','1.15L'),(9,'UAPS João Medeiros de Lima',1,-38.5969,-3.71416,'2018-11-14 04:34:52','18F',877,1,553,'',0,'','1.8F'),(10,'UAPS Alarico Leite',1,-38.5376,-3.81174,'2018-12-05 17:42:16','18F',3516,6,2147,'',0,'','1.8F'),(11,'UAPS Anísio Teixeira',1,-38.493,-3.85908,'2018-12-05 17:30:34','18F',2938,6,2031,'Oi',0,'8955316929883819597f','1.8F'),(12,'UAPS César Cals de Oliveira',1,-38.5125,-3.76469,'2018-12-05 17:30:11','18F',3385,6,2086,'Oi',2147483647,'8955316929883819514f','1.8F'),(13,'UAPS Dr. Otóni Cardoso do Vale',1,-38.4805,-3.84723,'2018-12-05 17:30:39','18F',1845,6,1289,'Oi',2147483647,'8955316929883819936f','1.8F'),(14,'UAPS Dr. Pompeu Vasconcelos',1,-38.5153,-3.82522,'2018-12-05 14:07:49','18F',2579,6,2111,'Oi',2147483647,'8955316929883819423f','1.8F'),(15,'UAPS Edilmar Norões',1,-38.5468,-3.80401,'2018-07-30 18:30:13','115L',1159,6,907,'Oi',2147483647,'8955316929883819340f','1.15L'),(16,'UAPS Edmar Fujita',1,-38.5181,-3.79326,'2018-12-05 17:29:49','18F',1431,6,1141,'',0,'','1.15L'),(17,'UAPS Evandro Ayres de Moura',1,-38.5238,-3.85219,'2018-12-05 17:30:32','115L',2061,6,1872,'',0,'','1.15L'),(18,'UAPS Francisco Melo Jaborandi',1,-38.5242,-3.83589,'2018-12-05 14:03:18','18F',3421,6,2393,'',0,'','1.8F'),(19,'UAPS Galba de Araújo',1,-38.4512,-3.81882,'2018-10-29 17:30:08','115L',2164,6,2006,'Oi',2147483647,'8955316929883819720f','1.15L'),(20,'UAPS Hélio Goes',1,-38.4758,-3.79093,'2018-12-05 19:11:38','18F',581,6,600,'',0,'','1.8F'),(21,'UAPS Janival de Almeida',1,-38.5195,-3.81236,'2018-10-23 17:26:48','18F',1626,6,1142,'',0,'','1.8F'),(22,'UAPS João Hipolito',1,-38.5275,-3.78845,'2018-11-14 16:23:22','115L',444,6,441,'',0,'','1.15L'),(23,'UAPS José Barros de Alencar',1,-38.5097,-3.8888,'2018-12-05 17:30:12','18F',3326,6,2080,'',0,'','1.8F'),(24,'UAPS Luis Franklin Pereira',1,-38.4843,-3.83749,'2018-09-17 13:08:34','115L',1760,6,1234,'',0,'','1.15L'),(25,'UAPS Manoel Carlos de Gouveia',1,-38.499,-3.78383,'2018-12-05 17:52:21','18F',1552,6,1008,'Oi',0,'8955316929883819290f','1.8F'),(26,'UAPS Marcos Aurélio Rabelo Lima Verde',1,-38.5116,-3.84154,'2018-09-25 17:30:03','115L',2214,6,1719,'Oi',0,'8955316929883819910f','1.15L'),(27,'UAPS Maria de Lourdes Jereissati',1,-38.5034,-3.78318,'2018-09-17 23:02:39','18F',1094,6,851,'',0,'','1.8F'),(28,'UAPS Maria Grasiela Teixeira Barroso',1,-38.5071,-3.86247,'2018-11-13 09:50:18','18F',3162,6,2176,'Oi',0,'8955316929883819407f','1.8F'),(29,'UAPS Mattos Dourado',1,-38.4786,-3.77236,'2018-12-05 17:45:11','18F',2459,6,1788,'Oi',0,'8955316929883819621f','1.8F'),(30,'UAPS Messejana',1,-38.4931,-3.83578,'2018-11-30 17:30:06','115L',1793,6,1186,'Oi',2147483647,'8955316929883819555f','1.15L'),(31,'UAPS Monteiro de Morais',1,-38.4671,-3.7979,'2018-12-05 13:52:24','18F',2510,6,2015,'',0,'','1.8F'),(32,'UAPS Osmar Viana',1,-38.5153,-3.85684,'2018-12-05 17:30:32','18F',2262,6,1906,'Oi',2147483647,'8955316929883819647f','1.8F'),(33,'UAPS Pedro Sampaio',1,-38.528,-3.84443,'2018-10-26 17:00:10','18F',1959,6,1504,'Oi',2147483647,'8955316929883820272f','1.8F'),(34,'UAPS Sítio São João',1,-38.5167,-3.8475,'2018-12-05 17:56:02','18F',2103,6,1563,'Oi',989316262,'8955316929883819761f','1.8F'),(35,'UAPS Terezinha Parente',1,-38.464,-3.82726,'2018-11-23 17:25:55','18F',4044,6,3169,'Oi',2147483647,'8955316929883819563f','1.8F'),(36,'UAPS Waldo Pessoa',1,-38.504,-3.8214,'2018-11-07 17:30:06','18F',4082,6,2785,'Oi',2147483647,'8955316929883819894f','1.8F'),(37,'UAPS Vicentina Campos',1,-38.5475,-3.79841,'2018-12-05 17:29:56','18F',2438,6,1933,'',0,'','1.15L'),(39,'UAPS Abner Cavalcante Brasil',1,-38.6086,-3.80471,'2018-10-05 07:23:28','18F',1369,5,857,'',0,'','1.8F'),(40,'UAPS Argeu Herbster',1,-38.6022,-3.79207,'2018-09-25 10:32:06','18F',2208,5,1661,'',0,'','1.8F'),(42,'UAPS Dom Lustosa',1,-38.6272,-3.78481,'2018-06-29 11:09:47','115L',1316,5,1153,'Oi',2147483647,'8955316929883820165f','1.15L'),(43,'UAPS Dr Pontes Neto',1,-38.6107,-3.77008,'2018-12-05 17:55:07','18F',857,5,521,'',0,'','1.8F'),(44,'UAPS Edmilson Pinheiro',1,-38.6178,-3.77702,'2018-12-05 17:45:10','18F',1212,5,957,'',0,'','1.8F'),(45,'UAPS Fernando Diógenes',1,-38.6018,-3.7808,'2018-09-26 12:21:23','115L',2140,5,1589,'',0,'','1.15L'),(46,'UAPS José Galba de Araújo',1,-38.6027,-3.75444,'2018-12-05 17:30:09','115L',3666,5,2969,'',0,'','1.15L'),(47,'UAPS Graciliano Muniz',1,-38.591,-3.81388,'2018-05-07 18:28:34','115L',1199,5,909,'',0,'','1.15L'),(48,'UAPS Guarany MontAlverne',1,-38.6206,-3.78593,'2018-12-05 16:00:28','18F',952,5,933,'',0,'','1.8F'),(49,'UAPS João Barbosa Pires de Paula Pessoa',1,-38.6209,-3.80393,'2018-11-09 12:24:29','18F',721,5,638,'',0,'','1.8F'),(50,'UAPS João Elisio Holanda',1,-38.5902,-3.82993,'2018-11-26 16:34:40','18F',1062,5,922,'',0,'','1.8F'),(51,'UAPS José Paracampos',1,-38.5804,-3.80775,'2018-11-23 13:38:35','18F',1855,5,1188,'',0,'','1.8F'),(52,'UAPS José Walter',1,-38.5612,-3.83617,'2018-12-05 17:30:10','115L',3318,5,1793,'Oi',2147483647,'8955316929883819993f','1.15L'),(53,'UAPS Jurandir Picanço',1,-38.6021,-3.77741,'2018-11-12 10:48:12','115L',3892,5,3628,'',0,'','1.15L'),(54,'UAPS Luciano Torres de Melo',1,-38.5769,-3.79152,'2018-12-05 17:55:21','18F',897,5,636,'',0,'','1.8F'),(55,'UAPS Luiza Távora',1,-38.5565,-3.81413,'2018-12-05 17:45:11','18F',2209,5,1378,'',0,'','1.8F'),(56,'UAPS Maciel de Brito',1,-38.6052,-3.76808,'2018-11-01 17:15:23','18F',502,5,399,'',0,'','1.8F'),(57,'UAPS Parque São José',1,-38.5873,-3.79682,'2018-12-05 17:30:10','18F',2930,5,2066,'',0,'','1.8F'),(58,'UAPS Pedro Celestino',1,-38.5695,-3.80604,'2018-10-31 11:30:02','18F',1828,5,1262,'Oi',0,'8955316929883819878f','1.8F'),(59,'UAPS Ronaldo Albuquerque Ribeiro',1,-38.597,-3.76825,'2018-12-05 17:30:08','18F',1501,5,729,'',0,'','1.8F'),(60,'UAPS Siqueira',1,-38.6186,-3.81423,'2018-10-18 11:41:28','18F',3142,5,2320,'',2147483647,'8955316929883820009f','1.8F'),(61,'UAPS Viviane Benevides',1,-38.5765,-3.80097,'2018-12-05 17:30:09','18F',2914,5,1989,'',0,'','1.15L'),(62,'UAPS Zélia Correia',1,-38.571,-3.83097,'2018-09-20 18:29:16','115L',1869,5,1535,'',0,'','1.15L'),(63,'UAPS Regina Maria da Silva Severino',1,-38.6016,-3.80736,'2018-12-05 17:55:15','18F',2428,5,2113,'Oi',2147483647,'8955316929883819530f','1.8F'),(64,'UAPS Abel Pinto',1,-38.5663,-3.7597,'2018-12-05 17:30:31','18F',1044,4,949,'',0,'','1.8F'),(66,'UAPS Francisco Monteiro (Chico Passeata)',1,-38.5559,-3.80125,'2018-08-24 18:30:12','115L',625,4,467,'',0,'','1.15L'),(68,'UAPS Gutemberg Brown',1,-38.5819,-3.7877,'2018-12-05 17:29:14','115L',825,4,497,'',0,'','1.15L'),(69,'UAPS Luis Costa',1,-38.5382,-3.74868,'2018-12-05 17:56:05','18F',2487,4,1657,'',0,'','1.8F'),(70,'UAPS Luiz Albuquerque Mendes',1,-38.5488,-3.77848,'2018-10-22 12:39:56','18F',928,4,641,'',0,'','1.8F'),(71,'UAPS Maria José Turbay Barreira',1,-38.5308,-3.76853,'2018-10-31 14:25:21','115L',1108,4,1047,'Oi',2147483647,'8955316929883819704f','1.15L'),(72,'UAPS Ocelo Pinheiro',1,-38.5555,-3.77108,'2018-09-06 15:26:53','115L',2053,4,1285,'',0,'','1.15L'),(73,'UAPS Oliveira Pombo',1,-38.5649,-3.75399,'2018-06-26 10:44:35','115L',687,4,556,'',0,'','1.15L'),(74,'UAPS Parangaba',1,-38.5567,-3.79518,'2018-06-12 16:18:20','1900Mo',615,4,522,'Oi',0,'8955316929883820066f','1.15L'),(76,'UAPS Roberto da Silva Bruno',1,-38.5278,-3.75992,'2018-12-05 17:55:09','18F',1490,4,939,'',0,'','1.8F'),(77,'UAPS José Valdevino de Carvalho',1,-38.559,-3.7762,'2018-12-05 17:24:32','115L',3943,4,2522,'',0,'','1.15L'),(78,'UAPS Dr. Gothardo Peixoto',1,-38.556,-3.76102,'2018-12-05 17:29:47','115L',3308,4,1833,'',0,'','1.15L'),(80,'UAPS César Cals de Oliveira Filho',1,-38.582,-3.7525,'2018-03-21 15:27:31','115L',201,3,98,'',0,'','1.15L'),(81,'UAPS Clodoaldo Pinto',1,-38.5809,-3.73075,'2018-12-05 17:30:33','115L',2178,3,1645,'',0,'','1.15L'),(82,'UAPS Eliézer Studart',1,-38.5957,-3.75485,'2018-10-05 18:54:22','115L',1684,3,1585,'',0,'','1.15L'),(83,'UAPS Fernandes Távora',1,-38.5907,-3.76371,'2018-11-05 12:49:17','115L',1998,3,1924,'',0,'','1.15L'),(84,'UAPS Francisco Pereira de Almeida',1,-38.5669,-3.74796,'2018-12-05 17:29:54','115L',5293,3,3095,'',0,'','1.15L'),(85,'UAPS George Benevides',1,-38.5986,-3.73674,'2018-12-05 17:45:05','18F',1112,3,1126,'',0,'','1.8F'),(86,'UAPS Hermínia Leitão',1,-38.5881,-3.73123,'2018-12-05 17:30:09','115L',1223,3,1145,'Oi',2147483647,'8955316929883819712f','1.15L'),(87,'UAPS Humberto Bezerra',1,-38.5913,-3.7406,'2018-12-05 17:30:03','18F',2037,3,1569,'Oi',2147483647,'8955316929883820066f','1.8F'),(88,'UAPS Ivana de Sousa Paes',1,-38.5695,-3.72505,'2018-12-05 17:26:15','115L',951,3,717,'',0,'','1.15L'),(89,'UAPS João XXIII',1,-38.5829,-3.77495,'2018-12-05 17:44:51','18F',2494,3,1746,'',0,'','1.15L'),(90,'UAPS Sobreira de Amorim',1,-38.5797,-3.76471,'2018-12-05 17:25:40','18F',1467,3,1438,'',0,'','1.8F'),(91,'UAPS Luis Recamonde Capelo',1,-38.5885,-3.77581,'2018-12-05 19:11:39','18F',2963,3,2190,'',0,'','1.8F'),(92,'UAPS Meton de Alencar',1,-38.5943,-3.7436,'2018-12-05 17:30:31','18F',3311,3,2190,'',0,'','1.8F'),(93,'UAPS Mariusa Silva de Sousa',1,-38.584,-3.78179,'2018-11-01 08:20:22','18F',1978,3,1227,'Oi',2147483647,'8955316929883819464f','1.8F'),(94,'UAPS Santa Liduína',1,-38.5457,-3.73968,'2018-10-26 09:48:39','18F',3552,3,2418,'Oi',2147483647,'8955316929883820264f','1.15L'),(95,'UAPS Waldemar de Alcantara',1,-38.5778,-3.77396,'2018-09-10 12:56:22','115L',1571,3,1217,'',0,'','1.15L'),(96,'UAPS Licínio Nunes de Miranda',1,-38.6007,-3.72761,'2018-12-05 17:29:55','18F',1550,3,1075,'Oi',0,'8955316929883819399f','1.15L'),(97,'UAPS Aída Santos e Silva',1,-38.4661,-3.72685,'2018-12-05 17:45:03','18F',1263,2,887,'',0,'','1.15L'),(98,'UAPS Benedito Arthur de Carvalho',1,-38.4956,-3.7693,'2018-11-01 17:24:10','18F',2433,2,2053,'',0,'','1.8F'),(99,'UAPS Célio Brasil Girão',1,-38.4649,-3.71816,'2018-12-05 17:45:16','18F',1983,2,1428,'Oi',2147483647,'8955316929883819852f','1.8F'),(100,'UAPS Flávio Marcílio',1,-38.4804,-3.72336,'2018-12-05 17:50:11','18F',481,2,283,'',0,'','1.8F'),(101,'UAPS Frei Tito',1,-38.4405,-3.76541,'2018-12-05 17:45:10','18F',1722,2,1553,'Oi',2147483647,'8955316929883819340f','1.15L'),(102,'UAPS Irmã Hercilia Aragão',1,-38.5176,-3.75592,'2018-10-18 11:05:52','18F',1270,2,666,'',0,'','1.8F'),(103,'UAPS Miriam Porto Mota',1,-38.4941,-3.74605,'2018-12-05 17:30:10','18F',1161,2,868,'',0,'','1.8F'),(104,'UAPS Odoríco de Morais',1,-38.4745,-3.72191,'2018-09-27 13:49:57','18F',1540,2,1070,'',0,'','1.8F'),(105,'UAPS Paulo Marcelo',1,-38.5224,-3.72934,'2018-12-03 11:59:31','18F',1800,2,1115,'Oi',2147483647,'8955316929883819613f','1.8F'),(106,'UAPS Pio XII',1,-38.5058,-3.75553,'2018-12-04 17:40:28','18F',1025,2,684,'',0,'','1.15L'),(107,'UAPS Rigoberto Romero',1,-38.4734,-3.75181,'2018-12-05 17:30:32','18F',3269,2,2050,'Oi',2147483647,'8955316929883819316f','1.8F'),(108,'UAPS Sandra Maria Faustino',1,-38.4714,-3.7284,'2018-12-05 17:30:02','18F',2414,2,2104,'Oi',2147483647,'8955316929883819985f','1.15L'),(109,'UAPS Airton Monte',1,-38.5778,-3.71383,'2018-09-27 13:23:23','115L',1450,1,1053,'',0,'','1.15L'),(110,'UAPS e CEO Carlos Ribeiro',1,-38.5469,-3.71946,'2018-10-30 03:03:34','115L',1761,1,1215,'',0,'','1.15L'),(111,'UAPS Casemiro Lima Filho',1,-38.5818,-3.70913,'2018-08-08 09:23:02','18F',8,1,4,'',0,'','1.8F'),(112,'UAPS e CEO Floresta',1,-38.5717,-3.71716,'2018-12-04 14:23:50','115L',1447,1,887,'',0,'','1.15L'),(113,'UAPS Fernando Façanha',1,-38.5812,-3.72467,'2018-12-05 18:46:29','18F',1210,1,871,'',0,'','1.8F'),(114,'UAPS Chico Domingos da Silva',1,-38.5744,-3.70641,'2018-12-05 17:30:10','18F',1406,1,915,'Oi',2147483647,'8955316929883819308f','1.8F'),(115,'UAPS Guiomar Arruda',1,-38.5456,-3.71341,'2018-06-14 15:16:28','115L',293,1,251,'',0,'','1.15L'),(117,'UAPS Lineu Jucá',1,-38.59,-3.70612,'2018-05-04 14:33:57','115L',1547,1,1163,'Oi',2147483647,'8955316929883819647f','1.15L'),(118,'UAPS Maria Aparecida Lima de Almeida',1,-38.5983,-3.71356,'2018-11-30 15:36:34','18F',791,1,570,'',0,'','1.8F'),(119,'UAPS Paulo de Melo Machado',1,-38.5575,-3.72154,'2018-11-11 14:53:42','18F',900,1,614,'',0,'','1.8F'),(120,'UAPS Projeto Quatro Varas',1,-38.5735,-3.70127,'2018-11-14 13:09:58','18F',134,1,76,'Oi',2147483647,'8955316929883820090f','1.8F'),(121,'UAPS Rebouças Macambira',1,-38.5908,-3.72177,'2018-12-05 17:45:04','18F',905,1,764,'',0,'','1.8F'),(122,'UAPS Virgílio Távora',1,-38.5641,-3.70547,'2018-11-22 21:05:53','18F',790,1,628,'',0,'','1.8F'),(127,'UAPS Maria Cirino Souza',1,-38.6157,-3.79809,'2018-05-22 00:00:00','1',0,1,1,'',0,'0','1.15L'),(128,'UAPS Dom Aloísio Lorscheider',1,-38.5566,-3.79522,'2018-12-05 19:11:39','18F',483,4,244,'',0,'','1.8F');
/*!40000 ALTER TABLE `local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `medico` (
  `idmedico` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `nascimento` date NOT NULL,
  `crm` int(11) NOT NULL,
  `salario` double NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  PRIMARY KEY (`idmedico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES (1,'Rodrigo Liporace','1995-05-10',12345,30000,'rodbala','balarod'),(2,'Hitalo Frota','2018-12-03',22222,550000,'hfrota','123456'),(4,'Antonio Fagundes','1977-06-21',101112,1000000,'fagundes','1010');
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sala`
--

DROP TABLE IF EXISTS `sala`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sala` (
  `idsala` int(11) NOT NULL AUTO_INCREMENT,
  `andar` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`idsala`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sala`
--

LOCK TABLES `sala` WRITE;
/*!40000 ALTER TABLE `sala` DISABLE KEYS */;
INSERT INTO `sala` VALUES (1,2,120),(2,3,8);
/*!40000 ALTER TABLE `sala` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-11 16:20:37
